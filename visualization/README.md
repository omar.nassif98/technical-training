# Data Dictionary
Explanation of the columns in the excel file

- **_unit_id:** Unique identifier
- **_golden:** Has been analyzed using the golden algorithm?
- **_last_judgment_at:** Date when the sentiment has been scored using the traditional algorithm. If blank, it has been analyzed using the golden algoritm (_golden = TRUE)
- **sentiment:** Sentiment score using traditional algorithm
- **sentiment:confidence:** Confidence that the score given by the traditional algorithm is acurate
- **sentiment_gold:** Sentiment score using golden algorithm
- **sentiment_gold_reason:** Explanation why the golden algorithm gave that specific score
- **text:** Analyzed text
