# technical-training
You will need to go through the following questions to be ready to start navigate through the data analysis world in
PlaceMe.

Answer the following questions in a new folder which you will name "answer". In this folder you will:
- Create a markdown document and write your answers there (except for the ones in *Data Visualization* section).
The answers do not need to be exhaustive, but it requires that you explain the concepts in your own words to make sure 
that you will handle them well when working in production.
- Regarding the Data Visualization question, preferably create a file using Jupyter notebook to include the Python code 
and your annotations.

Commit your files into the git repository creating a branch following this format
```
[first_name]_[last_name]
```

## Web development
- How does and HTTP request is processed from beginning to end (until the client receives a response)?
- What are RESTful services?
- How is the information structured in a JSON format?
- What are the HTTP methods and how they behave?
- How do you pass parameter to an HTTP GET request?
- How do you pass parameter to an HTTP POST request?
- How do you build a web interface using Flask framework in Python?
- How do you handle web requests Flask framework in Python?

## Redis
- What is redis?
- How do you store information in redis using Python?
- How do you extract information from redis using Python?

## Nginx
- What is nginx?
- How does nginx work?

## Docker
- What is docker?
- What is the difference between an image and a container?
- What will happen if you run the following command
```commandline
docker build redis
```
- Describe with your own words what the following files do
```
placemeanalytics\app\Dockerfile
placemeanalytics\nginx\Dockerfile
placemeanalytics\TenantDatabase\Dockerfile
```
- What is docker-compose?
- Describe with your own words what the following file does
```
placemeanalytics\docker-compose.prod.yml
```

## Data Visualization
- From the given dataset (visualization\av_sentiment_data.xlsx), perform a data analysis and support your analysis with data visualization using plotly (at least two visualizations).
- Use markdown to explain your thought process (use the following questions as a guide but not as a strict format of question & answers)
    - Question being answered
    - How you are approaching your analysis
    - Your considerations during the exploratory data analysis
    - Why you decided to do a specific visualization
    - What are your takeaways from that visualization
